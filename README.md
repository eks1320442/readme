#### Demo Project:
- Create AWS EKS cluster with a Node Group
Technologies used:
- Kubernetes, AWS EKS
#### Project Description:
- Configure necessary IAM Roles
- Create VPC with Cloudformation Template for Worker Nodes
- Create EKS cluster (Control Plane Nodes)
- Create Node Group for Worker Nodes and attach to EKS cluster 
- Configure Auto-Scaling of worker nodes
- Deploy a sample application to EKS cluster

aws eks update-kubeconfig --name eks-cluster-name

#### Create a Node-group-role with the following policies
- nodegroup
- amazonworker node policy
- container registry policy readonly
- amazonEKS_CNI

create autoscaling policy and attach to node-group-role

   cluster-autoscaler.kubernetes.io/safe-to-evict: "false"

- --balance-similar-node-groups
- -- skip-nodes-with-system-pods=false

Configure Docker Login

    kubectl create secret docker-registry my-docker-registry-key --docker-server=docker.io --docker-username=cholkuany --docker-password=xxxxx

    kubectl create secret docker-registry my-aws-registry-key --docker-server=1219696FB96340EF6CB76D51C6AAE09C.yl4.ca-central-1.ecr.amazonaws.com' --docker-username=AWS
    --docker-password=xxxxx

---

#### Demo Project:
- Create EKS cluster with Fargate profile

#### Technologies used:
- Kubernetes, AWS EKS, AWS Fargate

#### Project Description:
- Create Fargate IAM Role
- Create Fargate Profile
- Deploy an example application to EKS cluster using Fargate profile


---

#### Demo Project:
- Create EKS cluster with eksctl

#### Technologies used:
- Kubernetes, AWS EKS, Eksctl, Linux

#### Project Description:
- Create EKS cluster using eksctl tool that reduces the manual effort of creating an EKS cluster

---

#### Demo Project:
- CD - Deploy to EKS cluster from Jenkins Pipeline

#### Technologies used:
- Kubernetes, Jenkins, AWS EKS, Docker, Linux

#### Project Description:
- Install kubectl and aws-iam-authenticator on a Jenkins server
- Create kubeconfig file to connect to EKS cluster and add it on Jenkins server
- Add AWS credentials on Jenkins for AWS account authentication
- Extend and adjust Jenkinsfile of the previous CI/CD pipeline to configure connection to EKS cluster


---

#### Demo Project:
- CD - Deploy to LKE cluster from Jenkins Pipeline

#### Technologies used:
- Kubernetes, Jenkins, Linode LKE, Docker, Linux

#### Project Description:
- Create K8s cluster on LKE
- Install kubectl as Jenkins Plugin
- Adjust Jenkinsfile to use Plugin and deploy to LKE cluster


---

#### Demo Project:
- Complete CI/CD Pipeline with EKS and private DockerHub registry

#### Technologies used:
- Kubernetes, Jenkins, AWS EKS, Docker Hub, Java, Maven, Linux, Docker,Git

#### Project Description:
- Write K8s manifest files for Deployment and Service configuration
- Integrate deploy step in the CI/CD pipeline to deploy newly built application image from DockerHub private registry to the EKS cluster
- So the complete CI/CD project we build has the following configuration:
    a. CI step:Increment version
    b. CI step: Build artifact for Java Maven application
    c. CI step: Build and push Docker image to DockerHub
    d. CD step: Deploy new application version to EKS cluster
    e. CD step: Commit the version update


---

#### Demo Project:
- Complete CI/CD Pipeline with EKS and AWS ECR

#### Technologies used:
- Kubernetes, Jenkins, AWS EKS, AWS ECR, Java, Maven, Linux, Docker, Git

#### Project Description:
- Create private AWS ECR Docker repository
- Adjust Jenkinsfile to build and push Docker Image to AWS ECR
- Integrate deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry
- So the complete CI/CD project we build has the following configuration:
    a. CI step:Increment version
    b. CI step: Build artifact for Java Maven application
    c. CI step: Build and push Docker image to AWS ECR
    d. CD step: Deploy new application version to EKS cluster
    e. CD step: Commit the version update


---

Module 11: Kubernetes on AWS - EKS